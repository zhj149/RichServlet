package com.apache.rich.servlet.core.server.rest;


import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.apache.rich.servlet.core.server.rest.controller.URLController;

import com.apache.rich.servlet.common.enums.RequestMethodEnums;

/**
 * Routeable Controller mapping
 *
 * Author : wanghailing
 */
public class ControllerRouter {

    /**
     * first index layer. indexd by HttpMethod and URL terms length
     *
     * our max supportted URL terms is 512
     */
    private final URLMapper[][] mapper = new URLMapper[RequestMethodEnums.UNKOWN.ordinal()][512];

    /**
     * get specified resource's controller or null if it don't exist
     *
     * @param resource url anaylzed resource
     * @return controller instance or null if it don't exist
     */
    public URLController findURLController(HttpURLResource resource) {
        try {
            // we have checked the request method is supportted!. couldn't index overflow
            URLMapper slot = mapper[resource.requestMethod().ordinal()][resource.fragments().size()];
            if (slot != null)
                return slot.get(resource);
        } catch (ArrayIndexOutOfBoundsException ignored) {
            ignored.printStackTrace();
        }
        return null;
    }

    public synchronized boolean register(HttpURLResource resource, URLController controller) {
        Indexer indexer = new Indexer(resource);
        if (mapper[indexer.HttpMethodIndex][indexer.TermsIndex] == null)
            mapper[indexer.HttpMethodIndex][indexer.TermsIndex] = new URLMapper();
        return mapper[indexer.HttpMethodIndex][indexer.TermsIndex].register(resource, controller);
    }

    public synchronized void unregister(HttpURLResource resource) {
        Indexer indexer = new Indexer(resource);
        if (mapper[indexer.HttpMethodIndex][indexer.TermsIndex] != null)
            mapper[indexer.HttpMethodIndex][indexer.TermsIndex].unregister(resource);
    }

    static class Indexer {
        public int HttpMethodIndex;
        public int TermsIndex;

        public Indexer(HttpURLResource resource) {
            HttpMethodIndex = resource.requestMethod().ordinal();
            TermsIndex = resource.fragments().size();
        }
    }

    static class URLMapper {
    	
        private final Map<HttpURLResource, URLController> controller = new ConcurrentHashMap<HttpURLResource, URLController>(1024);

        public URLController get(HttpURLResource resource) {
            return controller.get(resource);
        }

        public boolean register(HttpURLResource resource, URLController controller) {
            return this.controller.put(resource, controller) == null;
        }

        public void unregister(HttpURLResource resource) {
            controller.remove(resource);
        }
    }
}
