/**
 * 
 */
package com.apache.rich.servlet.core.server.helper;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @author wanghailing
 *
 */
public class RichServletServerOptionProvider {
	
	private Map<RichServletServerOptions<?>, Object> options = new ConcurrentHashMap<>();

    public <T> RichServletServerOptionProvider option(RichServletServerOptions<T> option, T value) {
        this.options.put(option, value);
        return this;
    }

    /**
     * return specified option name's value if this option is setted. or named option
     * default value.
     *
     * @param option named option already defined
     * @return setted value or default value
     */
    @SuppressWarnings("unchecked")
    public <T> T option(RichServletServerOptions<T> option) {
        T value;
        if ((value = (T) options.get(option)) != null)
            return value;
        else
            return option.defaultValue();
    }
}	
