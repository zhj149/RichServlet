/**
 * 
 */
package com.apache.rich.servlet.http.servlet.realize;

import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * PrintWriter 实现类
 * @author wanghailing
 *
 */
public class RichServletHttpPrintWriter extends PrintWriter{
	
	private boolean flushed = false;

    public RichServletHttpPrintWriter(OutputStream out) {
        super(out);
    }

    @Override
    public void flush() {
        super.flush();
        this.flushed = true;
    }

    public boolean isFlushed() {
        return flushed;
    }
}
