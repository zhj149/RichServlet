package com.apache.rich.servlet.common.httpclient;

import com.apache.rich.servlet.common.http.listener.HttpDownloadListener;
import com.apache.rich.servlet.common.httpclient.HttpClient;
import com.apache.rich.servlet.common.httpclient.HttpRequest;
import com.apache.rich.servlet.common.httpclient.HttpResponse;
import com.apache.rich.servlet.common.httpclient.impl.HttpClientUrlConnectionImpl;
import java.io.File;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public abstract class HttpUtils {
	public static final boolean DEBUG = false;

	public static HttpClient getHttpClient() {
		return new HttpClientUrlConnectionImpl();
	}

	public static HttpResponse postJson(String url, String bodyJson) {
		HttpClient client = getHttpClient();
		HttpRequest request = new HttpRequest();
		request.setUrl(url);
		request.putBodyJson(bodyJson);
		printLog(request);
		return client.postJson(request);
	}

	public static HttpResponse post(String url, Map<String, String> paramsMap) {
		HttpClient client = getHttpClient();
		HttpRequest request = new HttpRequest();
		request.setUrl(url);
		if (null != paramsMap) {
			Iterator arg3 = paramsMap.entrySet().iterator();

			while (arg3.hasNext()) {
				Entry entry = (Entry) arg3.next();
				request.putParam((String) entry.getKey(),
						(String) entry.getValue());
			}
		}

		printLog(request);
		return client.post(request);
	}

	public static HttpResponse get(String url, Map<String, String> paramsMap) {
		HttpClient client = getHttpClient();
		HttpRequest request = new HttpRequest();
		request.setUrl(url);
		if (null != paramsMap) {
			Iterator arg3 = paramsMap.entrySet().iterator();

			while (arg3.hasNext()) {
				Entry entry = (Entry) arg3.next();
				request.putParam((String) entry.getKey(),
						(String) entry.getValue());
			}
		}

		printLog(request);
		return client.get(request);
	}

	public static HttpResponse upload(String url, Map<String, File> fileMap,
			Map<String, String> paramMap) {
		HttpClient client = getHttpClient();
		HttpRequest request = new HttpRequest();
		request.setUrl(url);
		Iterator arg4;
		Entry entry;
		if (null != fileMap) {
			arg4 = fileMap.entrySet().iterator();

			while (arg4.hasNext()) {
				entry = (Entry) arg4.next();
				request.putFile((String) entry.getKey(),
						(File) entry.getValue());
			}
		}

		if (null != paramMap) {
			arg4 = paramMap.entrySet().iterator();

			while (arg4.hasNext()) {
				entry = (Entry) arg4.next();
				request.putParam((String) entry.getKey(),
						(String) entry.getValue());
			}
		}

		printLog(request);
		return client.upload(request);
	}

	public static HttpResponse getDowload(String url,
			Map<String, String> paramsMap, String saveFileName,
			HttpDownloadListener downloadListener) {
		HttpClient client = getHttpClient();
		HttpRequest request = new HttpRequest();
		request.setUrl(url);
		request.setDownloadFileName(saveFileName);
		request.setDownloadListener(downloadListener);
		if (null != paramsMap) {
			Iterator arg5 = paramsMap.entrySet().iterator();

			while (arg5.hasNext()) {
				Entry entry = (Entry) arg5.next();
				request.putParam((String) entry.getKey(),
						(String) entry.getValue());
			}
		}

		printLog(request);
		return client.getDowload(request);
	}

	public static HttpResponse postDowload(String url,
			Map<String, String> paramsMap, String saveFileName,
			HttpDownloadListener downloadListener) {
		HttpClient client = getHttpClient();
		HttpRequest request = new HttpRequest();
		request.setUrl(url);
		request.setDownloadFileName(saveFileName);
		request.setDownloadListener(downloadListener);
		if (null != paramsMap) {
			Iterator arg5 = paramsMap.entrySet().iterator();

			while (arg5.hasNext()) {
				Entry entry = (Entry) arg5.next();
				request.putParam((String) entry.getKey(),
						(String) entry.getValue());
			}
		}

		printLog(request);
		return client.postDowload(request);
	}

	private static void printLog(HttpRequest request) {
	}
}