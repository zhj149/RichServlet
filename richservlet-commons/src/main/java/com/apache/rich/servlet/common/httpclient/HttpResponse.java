package com.apache.rich.servlet.common.httpclient;

import java.io.File;

public class HttpResponse {
	public static final int STATUS_CODE_SUCCESS = 200;
	public static final int STATUS_CODE_FAIL = -1;
	private int statusCode;
	private String reasonPhrase;
	private String resultStr;
	private File resultFile;

	public HttpResponse() {
		this(-1, (String) null);
	}

	public HttpResponse(int statusCode) {
		this(statusCode, (String) null);
	}

	public HttpResponse(int statusCode, String reasonPhrase) {
		this.statusCode = statusCode;
		this.reasonPhrase = reasonPhrase;
	}

	public boolean isStatusOk() {
		return 200 == this.statusCode;
	}

	public int getStatusCode() {
		return this.statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getReasonPhrase() {
		return this.reasonPhrase;
	}

	public void setReasonPhrase(String reasonPhrase) {
		this.reasonPhrase = reasonPhrase;
	}

	public String getResultStr() {
		return this.resultStr;
	}

	public void setResultStr(String resultStr) {
		this.resultStr = resultStr;
	}

	public File getResultFile() {
		return this.resultFile;
	}

	public void setResultFile(File resultFile) {
		this.resultFile = resultFile;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("statusCode[" + this.statusCode + "]");
		sb.append("reasonPhrase[" + this.reasonPhrase + "]");
		sb.append("resultStr[" + this.resultStr + "]");
		return sb.toString();
	}
}