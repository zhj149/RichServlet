/**
 * 
 */
package com.apache.rich.servlet.http2.server.acceptor;

import java.security.cert.CertificateException;

import javax.net.ssl.SSLException;

import com.apache.rich.servlet.http2.server.handler.RichServletHttp2OrHttpHandler;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.ssl.ApplicationProtocolConfig;
import io.netty.handler.ssl.ApplicationProtocolNames;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.SupportedCipherSuiteFilter;
import io.netty.handler.ssl.ApplicationProtocolConfig.Protocol;
import io.netty.handler.ssl.ApplicationProtocolConfig.SelectedListenerFailureBehavior;
import io.netty.handler.ssl.ApplicationProtocolConfig.SelectorFailureBehavior;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import static io.netty.handler.codec.http2.Http2SecurityUtil.CIPHERS;
import com.apache.rich.servlet.core.server.RichServletServer;
import com.apache.rich.servlet.core.server.helper.RichServletServerOptionProvider;
import com.apache.rich.servlet.core.server.acceptor.AsyncAcceptor;

/**
 * @author wanghailing
 *
 */
public class RichServletHttp2ServerAsyncAcceptor extends AsyncAcceptor {

	public RichServletHttp2ServerAsyncAcceptor(
			RichServletServer richServletServer) {
		super(richServletServer);
	}

	/* (non-Javadoc)
	 * @see com.apache.rich.servlet.core.server.acceptor.AsyncAcceptor#protocolPipeline(io.netty.channel.ChannelPipeline, com.apache.rich.servlet.core.server.helper.RichServletServerOptionProvider)
	 */
	@Override
	protected void protocolPipeline(SocketChannel ch,
			RichServletServerOptionProvider options) throws Exception{
			final SslContext sslCtx = configureTLS();
			ch.pipeline().addLast(sslCtx.newHandler(ch.alloc()), new RichServletHttp2OrHttpHandler(options));
			
	}
	
	private static SslContext configureTLS() throws CertificateException,SSLException{
		SelfSignedCertificate ssc = new SelfSignedCertificate();
		ApplicationProtocolConfig apn = new ApplicationProtocolConfig(Protocol.ALPN,
				// NO_ADVERTISE is currently the only mode supported by both
				// OpenSsl and JDK providers.
				SelectorFailureBehavior.NO_ADVERTISE,
				// ACCEPT is currently the only mode supported by both OpenSsl
				// and JDK providers.
				SelectedListenerFailureBehavior.ACCEPT, ApplicationProtocolNames.HTTP_2,
				ApplicationProtocolNames.HTTP_1_1);

		return SslContextBuilder.forServer(ssc.certificate(), ssc.privateKey(), null)
				.ciphers(CIPHERS, SupportedCipherSuiteFilter.INSTANCE).applicationProtocolConfig(apn).build();
		
	}
}
