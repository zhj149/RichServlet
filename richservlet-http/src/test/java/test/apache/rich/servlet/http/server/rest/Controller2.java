package test.apache.rich.servlet.http.server.rest;

import com.alibaba.fastjson.JSONObject;

import com.apache.rich.servlet.common.enums.RequestMethodEnums;
import com.apache.rich.servlet.core.annotation.ResponseBody;
import com.apache.rich.servlet.core.annotation.RequestMapping;
import com.apache.rich.servlet.core.annotation.RequestParam;
import com.apache.rich.servlet.core.annotation.Controller;


@Controller
public class Controller2 {

    @RequestMapping(value = "/rds/bind", method = RequestMethodEnums.POST)
    public @ResponseBody
    HttpResult<Void> bind(
            @RequestParam("tenantId") Long tenantId,
            @RequestParam("instanceId") String instanceId,
            @RequestParam("projectId") Long projectId) {
        return new HttpResult();
    }

    @RequestMapping(value = "/rds/unbind", method = RequestMethodEnums.POST)
    public 
    JSONObject unbind(
            @RequestParam("tenantId") Long tenantId,
            @RequestParam("instanceId") String instanceId) {
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("tenantId", tenantId);
        jsonObject.put("instanceId", instanceId);

        return jsonObject;
    }

    @RequestMapping(value = "/rds/project", method = RequestMethodEnums.GET)
    public 
    JSONObject getBindProject(
            @RequestParam("tenantId") Long tenantId,
            @RequestParam("instanceId") String instanceId) {
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("tenantId", tenantId);
        return jsonObject;
    }
}